import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminComponent } from './admin/admin.component';
import { NgxDatatableComponent } from './admin/pages/ngx-datatable/ngx-datatable.component';
import { WebixComponent } from './admin/pages/webix/webix.component';

const routes: Routes = [
  {
    path: 'Admin',
    component: AdminComponent,
    children: [
      { path: 'NgxDatatable', component: NgxDatatableComponent },
      { path: 'Webix', component: WebixComponent },
      { path: '', component: NgxDatatableComponent, pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: '/Admin', pathMatch: 'full' },
  { path: 'NotFound', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
