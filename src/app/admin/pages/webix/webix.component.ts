import { Component, OnInit, ElementRef, AfterContentInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Webix } from 'webix-angular';

declare var webix: any;

@Component({
  selector: 'app-webix',
  templateUrl: './webix.component.html',
  styleUrls: ['./webix.component.css']
})
export class WebixComponent implements OnInit, OnDestroy, AfterContentInit {
 public treeb: Webix.ui.tree;
 private root: ElementRef;

 smallTreeData = [
	{ id: 'root', value: 'Films data', open: true, data: [
	{ id: '1', open: true, value: 'The Shawshank Redemption', data: [
	{ id: '1.1', value: 'Part 1' },
	{ id: '1.2', value: 'Part 2', data: [
	{ id: '1.2.1', value: 'Page 1' },
	{ id: '1.2.2', value: 'Page 2' },
	{ id: '1.2.3', value: 'Page 3' },
	{ id: '1.2.4', value: 'Page 4' },
	{ id: '1.2.5', value: 'Page 5' }
]},
				{ id: '1.3', value: 'Part 3' }
			]},
			{ id: '2', open: true, value: 'The Godfather', data: [
				{ id: '2.1', value: 'Part 1' },
				{ id: '2.2', value: 'Part 2' }
			]}
		]}
	];

 constructor(private _titleService: Title, root: ElementRef) {
  this.root = root;
 }

 ngAfterContentInit() {
  this.webixLoad();
 }

 ngOnInit() {
  this._titleService.setTitle('Webix Page');
  // this.treeb.resize();
 }

 webixLoad(): void {
		this.treeb = <Webix.ui.tree> webix.ui({
			container:  this.root.nativeElement.querySelector('div#testB'),
			view: 'tree',
			select: 'multiselect',
			drag: true,
			data: this.smallTreeData
		});
 }

 ngOnDestroy() {
  // this.treeb.destructor();
 }

}
